// Import thư viện Express Js
const express = require("express");
// Khai báo thư viện mongo
const mongoose = require("mongoose");
const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");

// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

app.use('/', courseRouter);
app.use('/', reviewRouter);

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
   });
// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});